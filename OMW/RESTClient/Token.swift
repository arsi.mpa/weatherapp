//
//  Token.swift
//  WeatherApp
//
//  Created by Arsalan Khan on 10/04/2020.
//  Copyright © 2020 Arsalan Khan. All rights reserved.
//

import Foundation
import TinyKeychain


struct Token: Codable {
    let authToken: String
}

extension Token {
    
    var token : String? {
        return Keychain.default[.authToken]?.authToken
    }
    
    func save() -> Bool {
        switch Keychain.default.storeObject(self, forKey: .authToken) {
            case .success:
                return true
            case .error(let error):
                print("\(error.description)")
                return false
        }
    }
    
    func deleteToken() -> Bool {
        
        switch Keychain.default.deleteObject(forKey: .authToken) {
        case .success:
            return true
        case .error(let error):
            print(error)
            return false
        }
    }
}

extension Keychain {
    static var `default` = Keychain(group: (Bundle.main.infoDictionary!["keychainAccessGroup"] as! String))
}

extension Keychain.Key {
    static var authToken: Keychain.Key<Token> {
        return Keychain.Key<Token>(rawValue: "auth.token.key.release", synchronize: true)
    }
}
