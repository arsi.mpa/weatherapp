//
//  HeaderCollectionViewCell.swift
//  OMW
//
//  Created by Arsalan Khan on 28/03/2020.
//  Copyright © 2020 Arsalan Khan. All rights reserved.
//

import UIKit

class HeaderCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblTemp: UILabel!
    @IBOutlet weak var lblWind: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    
}
