//
//  WeatherForecast.swift
//  OMW
//
//  Created by Arsalan Khan on 10/04/2020.
//  Copyright © 2020 Arsalan Khan. All rights reserved.
//

import Foundation

// MARK: - ForecastReport
struct ForecastReport: Codable {
    let cod: String
    let message, cnt: Int
    let list: [ForecastList]
    let city: ForecastCity
}

// MARK: - ForecastCity
struct ForecastCity: Codable {
    let id: Int
    let name: String
    let coord: ForecastCoord
    let country: String
    let population, timezone, sunrise, sunset: Int
}

// MARK: - ForecastCoord
struct ForecastCoord: Codable {
    let lat, lon: Double
}

// MARK: - ForecastList
struct ForecastList: Codable {
    let dt: Int
    let main: ForecastMainClass
    let weather: [ForecastWeather]
    let clouds: ForecastClouds
    let wind: ForecastWind
    let sys: ForecastSys
    let dtTxt: String
    let rain: ForecastRain?

    enum CodingKeys: String, CodingKey {
        case dt, main, weather, clouds, wind, sys
        case dtTxt = "dt_txt"
        case rain
    }
}

// MARK: - ForecastClouds
struct ForecastClouds: Codable {
    let all: Int
}

// MARK: - ForecastMainClass
struct ForecastMainClass: Codable {
    let temp, feelsLike, tempMin, tempMax: Double
    let pressure, seaLevel, grndLevel, humidity: Int
    let tempKf: Double

    enum CodingKeys: String, CodingKey {
        case temp
        case feelsLike = "feels_like"
        case tempMin = "temp_min"
        case tempMax = "temp_max"
        case pressure
        case seaLevel = "sea_level"
        case grndLevel = "grnd_level"
        case humidity
        case tempKf = "temp_kf"
    }
}

// MARK: - ForecastRain
struct ForecastRain: Codable {
    let the3H: Double

    enum CodingKeys: String, CodingKey {
        case the3H = "3h"
    }
}

// MARK: - ForecastSys
struct ForecastSys: Codable {
    let pod: ForecastPod
}

enum ForecastPod: String, Codable {
    case d = "d"
    case n = "n"
}

// MARK: - ForecastWeather
struct ForecastWeather: Codable {
    let id: Int
    let main: ForecastMainEnum
    let weatherDescription: ForecastDescription
    let icon: String

    enum CodingKeys: String, CodingKey {
        case id, main
        case weatherDescription = "description"
        case icon
    }
}

enum ForecastMainEnum: String, Codable {
    case clear = "Clear"
    case clouds = "Clouds"
    case rain = "Rain"
}

enum ForecastDescription: String, Codable {
    case brokenClouds = "broken clouds"
    case clearSky = "clear sky"
    case fewClouds = "few clouds"
    case lightRain = "light rain"
    case overcastClouds = "overcast clouds"
    case scatteredClouds = "scattered clouds"
    
    var desc : String {
        return rawValue
    }
}

// MARK: - ForecastWind
struct ForecastWind: Codable {
    let speed: Double
    let deg: Int
}
