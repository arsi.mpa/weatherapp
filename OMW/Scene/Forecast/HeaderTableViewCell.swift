//
//  HeaderTableViewCell.swift
//  OMW
//
//  Created by Arsalan Khan on 28/03/2020.
//  Copyright © 2020 Arsalan Khan. All rights reserved.
//

import UIKit

class HeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
