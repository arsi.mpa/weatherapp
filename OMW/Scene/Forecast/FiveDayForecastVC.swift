//
//  FiveDayForecastVC.swift
//  OMW
//
//  Created by Arsalan Khan on 10/04/2020.
//  Copyright © 2020 Arsalan Khan. All rights reserved.
//

import UIKit
import MBProgressHUD
import Kingfisher

protocol ForecastDisplayable: class {
    func displayForecast(viewModel: ForecastViewModel.ViewModelArray)
    func displayLoader()
    func dislpayError(error: String)
}

class FiveDayForecastVC: UIViewController {
 
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var foreCastReport: ForecastReport? = nil
    
    var viewModel: ForecastViewModel.ViewModelArray?
    
    var interactor: ForecastInteractor? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Forecast"
        
        let presenter = ForecastPresenter(viewController: self)
        interactor = ForecastInteractor(presenter: presenter)
        
        interactor?.askPermissionForLocation()
    }
}

extension FiveDayForecastVC : UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel?.viewModels.first?.hourlyModel.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "topCell", for: indexPath) as! HeaderCollectionViewCell
        
        let forecast = viewModel!.viewModels.first!.hourlyModel[indexPath.row]
        
        cell.lblTemp.text = forecast.temp
        cell.lblWind.text = forecast.wind
        cell.lblTime.text = forecast.time
        cell.imgView.kf.setImage(with: URL(string: forecast.icon)!)
        
        return cell
    }
}

extension FiveDayForecastVC : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.viewModels.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ForecastTopCell
        
        let cast = viewModel!.viewModels[indexPath.row]
        
        cell.lblMIn.text = cast.avgTempMin
        cell.lblMax.text = cast.avgTempMax
        cell.lblWind.text = cast.avgWind
        cell.lblDesc.text = cast.desc
        cell.lblDate.text = cast.date
        cell.imgView.kf.setImage(with: URL(string: cast.icon)!)
        
        return cell
    }
}

extension FiveDayForecastVC : ForecastDisplayable {
    
    func displayLoader() {
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.hide(animated: true, afterDelay: 30)
    }
    
    func dislpayError(error: String) {
        
        MBProgressHUD.hide(for: self.view, animated: true)
        
        showAlertWithTitle(title: "Error", message: error, okButtonTitle: "OK")
    }
    
    func displayForecast(viewModel: ForecastViewModel.ViewModelArray) {
        
        MBProgressHUD.hide(for: self.view, animated: true)
        
        self.viewModel = viewModel
        
        self.tableView.reloadData()
        self.collectionView.reloadData()
    }
}
