//
//  ForecastTopCell.swift
//  OMW
//
//  Created by Arsalan Khan on 28/03/2020.
//  Copyright © 2020 Arsalan Khan. All rights reserved.
//

import UIKit

class ForecastTopCell: UITableViewCell {
    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblWind: UILabel!
    @IBOutlet weak var lblMIn: UILabel!
    @IBOutlet weak var lblMax: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
