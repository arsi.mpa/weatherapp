//
//  LandingViewController.swift
//  OMW
//
//  Created by Arsalan Khan on 10/04/2020.
//  Copyright © 2020 Arsalan Khan. All rights reserved.
//

import UIKit

class LandingViewController: UIViewController {

    @IBOutlet weak var txtFieldCities: UITextField!
    @IBOutlet weak var btnCheckWeather: UIButton!
    
    private var queryString: String?
    
    let fileManager: LocalFileManager = LocalFileManager()
    let validator: CitiesValidator = CitiesValidator()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Test"
    }
    
    @IBAction func btnCheckWeatherTapped(_ sender: UIButton) {
                    
        let strArray = txtFieldCities.text?.components(separatedBy: ",")
                
        if !validator.validateString(array: strArray) {
            
            return
        }
        
        fileManager.getCitiesFromJSON(strArray: strArray) { (success, cityIds) in
            
            guard success else {
                
                DispatchQueue.main.async {
                    // Show Alert
                    let alertVc = UIAlertController(title: "Error", message: "Cities should be greater than 2 and less than 8", preferredStyle: .alert)
                    alertVc.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                        self.dismiss(animated: true, completion: nil)
                    }))
                    
                    self.present(alertVc, animated: true)
                }
                return
            }
            
            self.queryString = cityIds
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "segueToWeather", sender: nil)
            }
            
        }
        
    }

    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueToWeather" {
            let destination = segue.destination as! WeatherForecastVC
            destination.queryCities = queryString
        }
    }

}
