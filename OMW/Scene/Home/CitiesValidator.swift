//
//  CitiesValidator.swift
//  OMW
//
//  Created by Arsalan Khan on 10/04/2020.
//  Copyright © 2020 Arsalan Khan. All rights reserved.
//

import Foundation

struct CitiesValidator {
    
    func validateString(array: [String]?) -> Bool {
          
          guard let array = array else {
              return false
          }
          
          guard array.count >= 3 && array.count <= 7 else {
              return false
          }
          
          return true
      }
    
}
