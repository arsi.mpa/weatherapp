//
//  LocalFileManager.swift
//  OMW
//
//  Created by Arsalan Khan on 10/04/2020.
//  Copyright © 2020 Arsalan Khan. All rights reserved.
//

import Foundation

struct LocalFileManager {
    
    private func readFromFile() -> [[String: Any]]? {
        
        let path = Bundle.main.path(forResource: "city.list", ofType: "json", inDirectory: nil, forLocalization: nil)
        let fileManager = FileManager()
        
        var json: [[String: Any]]? = nil
        
        if fileManager.fileExists(atPath: path!) {

            do {
                let data = try Data.init(contentsOf: URL(fileURLWithPath: path!))
                json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [[String: Any]]
            }catch(let err) {
                print("cant open file Error: \(err)")
            }
        }
        
        return json
    }
    
    func getCitiesFromJSON(strArray: [String]?, completion: @escaping (Bool, String?) -> Void) {
                      
           DispatchQueue.global().async {
    
               let json = self.readFromFile()
                   
               let trimmedArray: [String]? = strArray?.map { $0.trim() }
               
               var cityIds = [String]()
               
               trimmedArray?.forEach({ (cityTxt) in
                   
                   if let filteredJSON = json?.first(where: { ($0["name"] as! String).lowercased() == cityTxt.lowercased() }) {
                       cityIds.append("\(filteredJSON["id"] ?? "")")
                   }
               })
               
               completion(true, cityIds.joined(separator: ","))
           }
       }
}
