//
//  UIAlert+ViewController.swift
//  OMW
//
//  Created by Arsalan Khan on 10/04/2020.
//  Copyright © 2020 Arsalan Khan. All rights reserved.
//

import UIKit

typealias AlertCompletionHandler =  ((_ completed: Bool) -> Void)?

extension UIViewController {
    
    /**
     Custom Method to show alert view across the app
     
     - parameter controller:        the view controller from where alert must be presented
     - parameter titleStr:          title String
     - parameter messageStr:        message String
     - parameter okButtonTitle:     ok Button Title-- if nil Okay button will still be added
     - parameter cancelButtonTitle: cancel Button Title--if nil only okay button will be added
     */
    func showAlertWithTitle( title:String, message:String , okButtonTitle: String = "Ok", cancelButtonTitle: String? = "Cancel", response : AlertCompletionHandler = .none) {
        
        let alertVC = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alertVC.addAction(okAction)
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let ok = UIAlertAction(title: okButtonTitle, style: .default, handler: { (action) in
            
            alertController.dismiss(animated: true, completion: nil)
            
            if let okResponse = response {
                okResponse(false)
            }
        })
        
        alertController.addAction(ok)
        
        let cancel = UIAlertAction(title: cancelButtonTitle , style: .cancel, handler: {(cancel) in
            alertController.dismiss(animated: true, completion: nil)
            if let cancelResponse = response {
                cancelResponse(true)
            }
        })
        
        alertController.addAction(cancel)
        
        DispatchQueue.main.async {
            self.present(alertVC, animated: true, completion: nil)
        }
    }
    
    func networkConnectivityErrorShown()  {
        showAlertWithTitle(title: "Error", message: "Please check your internet connectivity", okButtonTitle: "OK")
    }
}
