//
//  StringExtensions.swift
//  OMW
//
//  Created by Arsalan Khan on 10/04/2020.
//  Copyright © 2020 Arsalan Khan. All rights reserved.
//

import Foundation

extension String
{
    func trim() -> String
   {
    return self.trimmingCharacters(in: NSCharacterSet.whitespaces)
   }
}
