# Weather Assignment
This app shows wheather details for cities and provide 5 days forecast.

# Design Architecture

The app uses VIPER architecture as a design pattern to manage and strcuture code. 
VIPER is aflavor of CLEAN.

So, What is VIPER architecture?

VIPER is a backronym for View, Interactor, Presenter, Entity, and Router.

This architecture is based on Single Responsibility Principle which leads to a clean architecture.
View: The responsibility of the view is to send the user actions to the presenter and shows whatever the presenter tells it.
Interactor: This is the backbone of an application as it contains the business logic.
Presenter: Its responsibility is to get the data from the interactor on user actions and after getting data from the interactor, it sends it to the view to show it. It also asks the router/wireframe for navigation.
Entity: It contains basic model objects used by the Interactor.
Router: It has all navigation logic for describing which screens are to be shown when. It is normally written as a wireframe.

# App Structure

App has been divided into Scenes and there are three scenes in the whole application:

1. Landing(MVC)
2. Weather (MVC)
3. Forecast (VIPER)

Since the scenes Landing and Weather were very small so corcing them to follow VIPER would have been a overhead for such small classes.

# Networking

For network calls Apples URLSession has been used. A Client is written on top of URLSession.
The HttpClient fully supports Dependency Injection(DI), a very core concept of VIPER. DI has many advantages as it provides easy way to Mocking of API requests.

Mock Classes has been added which makes Unit Testing very easy and manageable in iOS.

# Run Unit Test

Unit tests have been provided for API Client, Token, and File Manager. Simple press "Command" + "U" to run all the tests.

# Run Application

To run the application simply press "Command" + "R"

# How to use Application

The application provided two action to user:

1. Check Weather for Multiple Cities
2. Check Forecast for current Location

1. If user wants to test Weather for Multiple Cities the application presents TextField which accepts comma separated Values.
User can input Min 3 and Max 7 cities at a time.

2. In order to use Forecast service, simply tap on "Use our 5 day forecast service" and allow the user lcoation when being presented.
It will detect the location and automatically resolve it to City name and get the forecast for it.


# Notes:

The application uses Metric System so all temperature units will be in Celcius(C) and for Wind it will Meter/Second(m/s).




