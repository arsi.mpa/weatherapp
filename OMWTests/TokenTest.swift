//
//  OMWTests.swift
//  OMWTests
//
//  Created by Arsalan Khan on 10/04/2020.
//  Copyright © 2020 Arsalan Khan. All rights reserved.
//

import XCTest
@testable import OMW


class TokenTests: XCTestCase {
    
    override func setUp() {
        
    }
    
    func testStore() {
        
        let sampleToken = "storeTest"
        
        let token = Token(authToken: sampleToken)
        
        let storageResult = token.save()
        
        if !storageResult {
            XCTFail("Storing object should not fail")
        }
        
        let storedToken = token.token!

        if storedToken.isEmpty {
            XCTFail("Should be able to retrieve object just after storage without error.")
        }

        XCTAssert(storedToken == sampleToken)
    }


    func testDelete() {
        
        let sampleToken = "storeTest"
        
        let token = Token(authToken: sampleToken)
        
        let storageResult = token.save()
        
        if !storageResult {
            XCTFail("Storing object should not fail")
        }
        

        if !token.deleteToken() {
            XCTFail("deleteing object should not fail")
        }
        
        XCTAssertNil(token.token, "Token should be nil immediately")
    }
}
